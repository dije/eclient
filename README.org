* fullemacsclient

Edit anything anywhere, anywhere you are, accessing one
remote always-running Emacs-server instance fully configured to your
satisfaction.

Were you perhaps first baffled, then disappointed, by the almost-but-not-quite
complete dissimilarity between stock =emacsclient= and client-server computing?
Then this is for you.

This script, and its suite of small auxiliary scripts, provides a
network-aware, network-transparent wrapper around stock =emacsclient=, if Emacs
server is running locally, and a drop-in substitute for =emacsclient=, if not.
It abstracts away the details of how TRAMP accesses remote files *and* of
which host tells Emacs what to do *and* of how to determine which X11 display
to use.  It allows you to enter the same command, with the same argument list
-- for example:

#+begin_src bash
  emacsclient ~/.profile
#+end_src

on either your local host or any host on which you run a shell session via
SSH, and have it Do What I Mean: namely, edit the file you want to edit, in
this case =${HOME}/.profile=, *whichever host it is on*, and have that specific
file presented to you wherever you are for editing via TRAMP in an Emacs frame
on your local X11 display.

It is possible to use the auxiliary script =ecc=, which essentially just adds the
command-line options =--quiet --create-frame=, as the value for the =EDITOR= and
=VISUAL= environment variables used by other programs for launching an
editor. This has been shown to work for example as the external editor for
=crontab -e=, for =git=, for =bzr=, for =less=, and for web pages' editable text in web
browsers such as Firefox, Chromium and Conkeror using extensions such as "Edit
with Emacs".  It even works for something as strict and fussy as =visudo=, using
the =sudo=-specific version =suec=:

#+begin_src bash
  sudo env VISUAL=suec visudo -f /etc/sudoers.d/local
#+end_src
While this script is hoped and intended to be fully cross-POSIX-platform,
please bear in mind that any Windows compatibility or support is both
incidental and accidental.

** Knowledge assumed:
   - SSH, specifically OpenSSH, PKI and the =keychain= program.
   - X11 forwarding over SSH.
   - What environment variables are and how they work, particularly =PATH=.
   - What symbolic links are and how they are made.
   - How to create a directory.
   - How to configure Emacs for both server mode and remote file access.
   - If you have superuser privileges, then how to configure =/etc/hosts=.

** Dependencies:

The script depends on the existence of the following:

   0. On one host of your choice: GNU Emacs, installed and permanently running in server mode using =--daemon=, with the standard supporting program =emacsclient= also installed.  This host, which we will call Host/Role E, may be one which you do not usually physically access.  The standard Emacs Lisp package for remote file access, TRAMP, must be loaded in the Emacs init file, as should the third-party package =keychain-environment=.

   1. OpenSSH public key infrastructure affording you transparent passwordless access between all hosts, plus the program =keychain= if any of your public keys requires a passphrase to use.  Setting this up is outside the scope of this manual.

   2. On each host whose screen and keyboard you may from time to time use -- we will call such hosts Host/Role X, generically -- you must be running the X Window System, aka X11.  Host/Role X already has this set up if it is running a non-server version of any Unix-like operating system other than Mac OS X.  Each SSH login connection you make from Host/Role X to Host/Role E and/or Host/Role F -- see below -- must have X11 forwarding enabled, using =ssh -X= (or =ssh -Y=).  Setting up X11 on Microsoft Windows or Mac OS X is outside the scope of this manual.

   3. On each host where you may wish to edit files and which you can login to using SSH -- generically, Host/Role F -- and also on both Host/Role E and Host/Role X, you need to create a directory under your home directory named =editors=.  This directory, =${HOME}/editors=, must be in your =PATH= environment variable and come *before* the directory that contains the "real" =emacsclient= in the =PATH= ordering.  (The latter directory is typically =/usr/bin/= or =/usr/local/bin/=).  This script must be placed (or linked to) in the former directory as =${HOME}/editors/emacsclient=.  How and why to configure your =PATH= environment variable for general usage is outside the scope of this manual.

   4. On Host/Role E, you must also create a symbolic link named =${HOME}/editors/_emacsclient= pointing to the "real" =emacsclient=.  Typically you set this up with the following command: =ln -s /usr/bin/emacsclient "${HOME}/editors/_emacsclient"= assuming =emacsclient= is installed at =/usr/bin/=.  On Host/Role X and Host/Role F you do *not* have to have Emacs installed, but if you do, you must also set up the =_emacsclient= link in the same way.

   5. Some preconfigured knowledge on each host (**** ??? ****)

   6. Associated scripts, each installed in some or other directory in the PATH:
      - trampify :: Uses a replacement pattern to make file pathnames formatted
        for TRAMP safe to pass through shell scripts on the way to being passed
        to the emacsclient binary.
      - ensure_emacs_server_file :: **** ? ****
      - emacs_server_is_running :: **** ? ****

   7. The Tenon C Shell, =tcsh=, compiled with the =-rh= option for remote-host detection -- see the output of =tcsh --version=.  By default, =tcsh= is built with this option on debian, MacPorts and Cygwin, which meets our requirement for a cross-platform solution.  =tcsh= is a dependency of the =remotehost_bash= script.  Any alternative method of detecting this information without the =tcsh= dependency would be most welcome.

** Configuration:

To use this script, you must have configured a passwordless SSH connection
with X11 forwarding enabled from Host/Role X, the host at which you sit, to
Host/Role E, the Emacs-server host.  You need a live SSH connection with X11
forwarding to a shell session on any Host/Role F.

In principle, Host/Role X, that is, your "seat", the host whose screen and
keyboard you are using for the time being, may from time to time be the same
host as Host/Role E, the Emacs-server host, provided Host/Role E is not a
"headless" server.  That is, you may sit down and physically use the screen
and keyboard of Host/Role E, at which point it also becomes Host/Role X.

In practice, Host/Role E and Host/Role X will each from time to time also be
the same host as Host/Role F.  That is, you will open files on Host/Role E and
Host/Role X in exactly the same way that you would open a file on any other
instance of Host/Role F.

In practice, Host/Role F may from time to time also be the same host as
Host/Role X, if and when you choose to physically use it for display and
input.  That is, when you sit down and physically use the screen and keyboard
of a host which you have previously thought of only as Host/Role F, that host
becomes for the time being Host/Role X.

The full Emacs package can be installed using the standard software repositories
of GNU/Linux distributions, *BSD, Cygwin on Microsoft Windows, and MacPorts on
Mac OS X, and doubtless many other package managers.

** Motivation

This script suite addresses three cross-cutting concerns:

   1. Access from Host/Role E to the specified file.  Callers such as the accompanying =ec*= scripts must use the accompanying =trampify= script to "pre-wrap" file arguments in what I am calling "trampiness", my custom shell-escaping convention for TRAMP filenames.  This script assumes that filename arguments passed to it are already thus "trampified" and thus, after un-escaping, ready for Emacs to find/visit/open.

   2. Access from Host/Role F's terminal command line to the stock =emacsclient= on Host/Role E (and no other).

   3. Access from Host/Role E to the appropriate X11 server (determined via =tcsh='s REMOTEHOST capability), assumed to be the one on the same host as the terminal session, Host/Role X. (Bear in mind that X11's server is local and client is remote).

** Negative implications:
   1. Use of =fullemacsclient= for non-editing purposes such as evaluating Elisp forms is not supported or guaranteed to work; no effort has been made in that direction.  Workaround: Call =_emacsclient= or use the absolute path to the "real" =emacsclient= under =/usr/=.

** Environment variables:
Mostly optional; the scripts will try to guess usable values if not given.
   - DEBUG_EC :: Verbose diagnostic output if this variable is set to =1=.
   - DISPLAY :: **** How is this set up on Host/Role E? ****
   - EMACS_HOST :: The IP address or host name of the Emacs server host.  Will
     default to =emacshost=.
   - EMACS_USER :: If you want to use =sudo= or =su= to invoke this script as =root=
     while running the Emacs server as a non- =root= user.
   - REMOTE_EMACS_USER :: The username of the Emacs process owner, if different
     to local =$USER=.
   - BIN_DIR :: =~/bin= or =~/.local/bin= or wherever you have it installed.
   - EMACS_UTILS_DIR :: =${BIN_DIR}/emacs-utils=
   - NETWORK_UTILS_DIR :: =${BIN_DIR}/network-utils=
   - EDITORS_DIR :: =~/editors= or wherever you have it installed.
   - PATH :: Must contain the directories =BIN_DIR=, =EMACS_UTILS_DIR=,
     =NETWORK_UTILS_DIR= and =EDITORS_DIR= at or near the beginning (before =/usr/...=).

* Symbolic links:
   - Inside =$EDITORS_DIR=:
     - emacsclient :: pointing to =$EMACS_UTILS_DIR/emacsclient=
     - emacsclient_display :: ditto
     - ec :: pointing to =$EMACS_UTILS_DIR/ec=
     - ecc :: ditto
     - ecn :: ditto
     - eccn :: pointing to =EMACS_UTILS_DIR/eccn=
     - emacs :: pointing to the "real" Emacs executable, for example
       =/usr/bin/emacs=.
     - _emacsclient :: pointing to the "real" =emacsclient= executable, for
       example =/usr/bin/emacsclient=.
     - emacsclient :: pointing to our script =$EMACS_UTILS_DIR/emacsclient=
     - suec :: pointing to =$EMACS_UTILS_DIR/suec=
   - Inside =$EMACS_UTILS_DIR=:
     - emacsclient_display :: pointing to =emacsclient=
     - ecc :: pointing to =ec=
     - ecn :: ditto
   - Inside =/= (if superuser-enabled; these enable the =suec= script, for superuser editing):
     - editors :: pointing to =$EDITORS_DIR=
     - emacs-utils :: pointing to =$EMACS_UTILS_DIR=
     - network-utils :: pointing to =$NETWORK_UTILS_DIR=

** Dependencies:

*** Our scripts:
    - refresh_keychain_environment.source.bash
    - emacs_server_is_on_localhost
    - remotehost_bash
    - trampify

*** Emacs libraries (loaded):
   - keychain-environment
   - tramp

*** Unix packages/commands:
   - OpenSSH
   - keychain
   - bash
   - tcsh, compiled to be REMOTEHOST aware (for the =remotehost_bash= script);
     this is usually the case in repo versions
   - psproc/psutils (for pgrep) (usually pre-installed)
   - getent (usually pre-installed)
   - sudo -- must be enabled
   - tty
   - grep
   - sed
   - who
   - xdotool
